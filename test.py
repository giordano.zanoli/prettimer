import unittest
import time
import logging

from prettimer import Elapsed, logruntime, timeit, logit


class TestElapsed(unittest.TestCase):

    def setUp(self) -> None:

        self.timer = Elapsed()

    def test_add_breakpoint(self):

        time.sleep(0.1)

        name = 'test_reset_false'
        self.timer.addBreakpoint(name=name, reset_last=False)
        self.assertNotEqual(first=self.timer.all_breakpoints['test_reset_false'],
                            second=self.timer.breakpoint,
                            msg=name)

        time.sleep(0.1)

        name = 'test_reset_true'
        self.timer.addBreakpoint(name=name, reset_last=True)
        self.assertAlmostEqual(first=self.timer.all_breakpoints['test_reset_true'],
                               second=self.timer.breakpoint,
                               msg=name, delta=1e-4)


class TestDecorators(unittest.TestCase):

    def setUp(self) -> None:

        class Params(object):
            def __init__(self, logger, timer):
                self.logger = logger
                self.timer = timer

        class LogruntimeWithDefault(object):

            def __init__(self, params):
                self.params = params

            @logruntime
            def mickey(self, n=10000000):
                return list(range(n))

        class Logruntime(object):

            def __init__(self, logger, timer):
                self.logger = logger
                self.timer = timer

            @logruntime(logger_attr='logger', timer_attr='timer')
            def mickey(self, n=10000000):
                return list(range(n))

        timer = Elapsed()
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.CRITICAL)
        formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s',
                                      "%Y-%m-%d %H:%M:%S")
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        @timeit
        def timeit_with_default(n=1000000):
            return list(range(n))

        @timeit(msg='MICKEY')
        def timeit_with_arg(n=1000000):
            return list(range(n))

        @logit()
        def logit_with_default(n=1000000):
            return list(range(n))

        @logit(logger_level=logger.info, msg='MICKEY')
        def logit_with_arg(n=1000000):
            return list(range(n))

        params = Params(logger, timer)

        self.logruntime_with_default = LogruntimeWithDefault(params)
        self.logruntime_with_arg = Logruntime(logger, timer)
        self.timeit_with_default = timeit_with_default
        self.timeit_with_arg = timeit_with_arg
        self.logit_with_default = logit_with_default
        self.logit_with_arg = logit_with_arg

    def test_logruntime_default_no_exception(self):
        # no exception raised test!
        self.logruntime_with_default.mickey()

    def test_logruntime_no_exception(self):
        # no exception raised test!
        self.logruntime_with_arg.mickey()

    def test_timeit_default_no_exception(self):
        # no exception raised test!
        self.timeit_with_default()

    def test_timeit_no_exception(self):
        # no exception raised test!
        self.timeit_with_arg()

    def test_logit_default_no_exception(self):
        # no exception raised test!
        self.logit_with_default()

    def test_logit_no_exception(self):
        # no exception raised test!
        self.logit_with_arg()


def run():

    elps = unittest.TestLoader().loadTestsFromTestCase(TestElapsed)
    deco = unittest.TestLoader().loadTestsFromTestCase(TestDecorators)

    suite = unittest.TestSuite([elps, deco])
    ret = unittest.TextTestRunner(verbosity=2).run(suite)

    return ret.wasSuccessful()


if __name__ == '__main__':

    run()
